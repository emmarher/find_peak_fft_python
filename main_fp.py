#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 11:41:47 2021

@author: emmarher

Objective: Find Peak

First create a sine wave with sampling interval pre-determined. we will combine two sine waves with frequencies 20 and 40. Remember high frequencies might be aliased if the time interval is large.

"""




# Import the necesary packages
from scipy import fftpack
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import numpy as np

#sampling freq in herts 20Hz, and 40Hz
freq_sampling1 = 2
freq_sampling2 =12
amplitude1 = 11 # amplitude of first sine wave
amplitude2 = 8 # amplitude of second sine wave
noise = np.random.normal(0,10,200)
time = np.linspace(0, 6, 200, endpoint=True) # time range with total samples of 500 from 0 to 6 with time intervals equals 6/500
y = amplitude1*np.sin(2*np.pi*freq_sampling1*time) + amplitude2*np.sin(2*np.pi*freq_sampling2*time)

# plt.figure(figsize = (20, 12))
# plt.plot(time, y, 'k',lw=0.8)
# plt.xlim(0,6)
# plt.show()

#  Apply fft  function
yf = fftpack.fft(y, time.size)

amp = np.abs(yf)# get amplitude spectrum
freq = np.linspace(0.0, 1.0/(2.0 *(6/200)), time.size//2) # get freq axis
f_energy = (2/amp.size)*amp[0:amp.size//2]




peaks, _ = find_peaks(f_energy, height=1.5)
#plot the amp spectrum

plt.figure(figsize=(20,12))
plt.plot(freq, f_energy)
plt.plot(freq[peaks], f_energy[peaks],"x")
plt.show()


# bandwidth
band1 = [0, 9]
band2 =[10,17]

for i in range(f_energy[peaks].size):

    if f_energy[peaks][i] >= band1[0] and f_energy[peaks][i]<=band1[1]:
        print( str(f_energy[peaks][i])+' Esta en la banda 1')
    elif f_energy[peaks][i] >= band2[0] and f_energy[peaks][i]<=band2[1]:
        print(str(f_energy[peaks][i])+' Esta en la banda 2')



